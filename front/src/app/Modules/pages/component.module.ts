import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ComponentsRoutes } from './component.routing';
import { BlogComponent } from './blog/blog.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { Ng2CompleterModule } from "ng2-completer";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BlogDetailsComponent } from './blog-details/blog-details.component';
import { AddBlogComponent } from './add-blog/add-blog.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ComponentsRoutes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule, 
    Ng2SmartTableModule, 
    Ng2CompleterModule, 
    FontAwesomeModule,
 
  ],
  declarations: [

    
    BlogComponent,
    BlogDetailsComponent,
    AddBlogComponent,

  ]
})
export class ComponentsModule {}
