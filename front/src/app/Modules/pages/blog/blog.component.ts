import { Component, OnInit } from '@angular/core';
import { timeout } from 'rxjs/operators';

import { BlogService } from 'src/app/Core/blog/blog.service';

import * as fa from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { Blog } from '../../shared/models/blog';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  fa = fa;
  showLoader:boolean=false 
  blogs:Blog[]=[];
  search:string=""; 




  constructor( private blogApiService:BlogService ,     private routes: Router, ) { }

  ngOnInit(): void {
        this.getAllBlog(); 
     
  }

  getAllBlog(){
    this.showLoader=true; 
  this.blogApiService.getAllBlog().subscribe(
        (data:any )=>{
          this.blogs=data.data;
          this.showLoader=false; 
        },
        error=>{
          console.error(error)
        }
    );
  }


  vote(blog:any,type:string){

    if(type=="up"){
      blog.upVoteNumber++
    }else if(type=="down") {
      blog.downVoteNumber++
    }
  
    this.blogApiService.voteBlog(blog.id,blog).subscribe(
      (data:any )=>{
       console.log("updated")
      },
      error=>{
        console.error(error)
      }
  );
  }



    ToDetails(id:any){

      this.routes.navigate(['/component/details-Blog/'+id ]);
    }

    toAddBlog(){
      this.routes.navigate(['/component/add-Blog/' ]);   
    }
     


    filter(word:string){
      this.blogs= this.blogs.filter((blog)=>{
        return blog.title.indexOf(word) >= 0 ; 
      });

      console.log(this.blogs)
    }


}
