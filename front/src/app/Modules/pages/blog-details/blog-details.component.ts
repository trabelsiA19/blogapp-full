import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as fa from '@fortawesome/free-solid-svg-icons';
import { BlogService } from 'src/app/Core/blog/blog.service';
import { Blog } from '../../shared/models/blog';

@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  styleUrls: ['./blog-details.component.css']
})
export class BlogDetailsComponent implements OnInit {
  fa = fa;
  blog:Blog=new Blog(); 
  constructor(private blogApiService:BlogService , private route: ActivatedRoute,private routes: Router, ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      if (params.get('_id')) {
        let id=params.get('_id'); 
        this.getblogById(id)


      }
    })
  }

  getblogById(id:any){
    this.blogApiService.getBlogById(id).subscribe(
      data=>{
        this.blog=data.data[0]
      },
      err=>{
        console.error(err)
      }

    )
  }

  Tolist(){
    this.routes.navigate(['/component/blog' ]);
  }
}
