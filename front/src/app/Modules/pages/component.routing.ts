import { Routes } from '@angular/router';
import { AddBlogComponent } from './add-blog/add-blog.component';
import { BlogDetailsComponent } from './blog-details/blog-details.component';

import { BlogComponent } from './blog/blog.component';




export const ComponentsRoutes: Routes = [
	{
		path: '',
		children: [
			{
				path: 'blog',
				component: BlogComponent,
				data: {
					title: 'blog',
					
				},
				
				
			},
			{
				path: 'details-Blog/:_id',
				component: BlogDetailsComponent,
				data: {
					title: 'details-blog',
					
				},
				
				
			},

			{
				path: 'add-Blog',
				component: AddBlogComponent,
				data: {
					title: 'add-blog',
					
				},
				
				
			},

		
		]
	}
];
