import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import * as fa from '@fortawesome/free-solid-svg-icons';
import { BlogService } from 'src/app/Core/blog/blog.service';
import { Blog } from '../../shared/models/blog';

@Component({
  selector: 'app-add-blog',
  templateUrl: './add-blog.component.html',
  styleUrls: ['./add-blog.component.css']
})
export class AddBlogComponent implements OnInit {
  fa = fa;
blog:Blog=new Blog(); 
  constructor( private blogApiService:BlogService ,private routes: Router, ) { }
  blogForm = new FormGroup({
    title: new FormControl(' '),
    author: new FormControl(' '),
    content: new FormControl(' '),
  });

  ngOnInit(): void {
  }


  ajouter(){

    console.log()


    this.blog.title=this.blogForm.value["title"]
    this.blog.author=this.blogForm.value["author"]
    this.blog.content=this.blogForm.value["content"]
    this.blogApiService.addBlog(this.blog).subscribe(
      newData => {
       this.Tolist()
 

            }, 
      error => {
        console.log(error);
      }
    ) 
  }


  Tolist(){
    this.routes.navigate(['/component/blog' ]);
  }
}
