import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
export const Approutes: Routes = [
  {
    path: '',
    children: [
    

      {
        path: 'component',
        loadChildren: () => import('./Modules/pages/component.module').then(m => m.ComponentsModule),
      },
      
    
    ]
  },

  {
    path: '**',
    redirectTo: 'component/blog'
  }
];
