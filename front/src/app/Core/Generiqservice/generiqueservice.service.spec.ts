import { TestBed } from '@angular/core/testing';

import { GeneriqueService } from './generiqueservice.service';

describe('GeneriqueserviceService', () => {
  let service: GeneriqueService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GeneriqueService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
