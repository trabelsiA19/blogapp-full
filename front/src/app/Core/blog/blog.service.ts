import { Injectable } from '@angular/core';
import { GeneriqueService } from '../Generiqservice/generiqueservice.service';

@Injectable({
  providedIn: 'root'
})
export class BlogService {


  constructor(public generiqueService :GeneriqueService ) { }
 /**
   * web services to get all blogs
   *
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */

  public getAllBlog( observe: any = 'body', reportProgress: boolean = false ) {

    return this.generiqueService.GetApi(observe,reportProgress, 'getAllBlogs')
  
  }



 /**
   * web services to create blog
   *
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */

  public addBlog( observe: any = 'body', reportProgress: boolean = false ) {

    return this.generiqueService.PostApi(observe,reportProgress, 'addBlog')
  
  }






 /**
   * web services to get blog by id
   *
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   * @param blogid id blog
   */

  public getBlogById( blogid : string  , observe: any = 'body', reportProgress: boolean = false ) {

    return this.generiqueService.GetApi(observe,reportProgress, `getBlog/${blogid}`)
  
  }



  /**
  * * web services to vote Blog 
  * 
  * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
  * @param reportProgress flag to report request and response progress.
  * @param blogid id blog
  */
   public voteBlog(  blogid : string  ,observe: any = 'body', reportProgress: boolean = false ){
    return this.generiqueService.PutApi(observe,reportProgress, `vote/${blogid}`)
   
  }


}
