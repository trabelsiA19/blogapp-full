import imp
from fastapi import APIRouter


from models.blog import Blog
from config.db import collection_name 
from schemas.blog import Blog_serializer,blogs_serializer
from bson import ObjectId

blog_router=APIRouter()

#
#  get All blogs 
#
@blog_router.get("/getAllBlogs")
async def find_all_blogs():
    allBlogs=blogs_serializer(collection_name.find())
    return {"status":"ok","data":allBlogs}

#
#  get one blog by id 
#
@blog_router.get("/getBlog/{id}")
async def get_blog(id:str):
     blog= blogs_serializer(collection_name.find({"_id": ObjectId(id) }))
     return  {"status":"ok","data":blog}



#
#  add  one blog  
#
@blog_router.post("/addBlog")
async def post_blog(blog:Blog):
    _id=collection_name.insert_one(dict(blog))
    blog= blogs_serializer(collection_name.find({"_id": _id.inserted_id }))
    return  {"status":"ok","data":blog}



# 
#  vote up or down  blog by id   
#
@blog_router.put("/vote/{id}")
async def up_Vote_Blog(id: str, blog: Blog):
    collection_name.find_one_and_update({"_id": ObjectId(id)}, {
        "$set": dict(blog)
    })
    blog=blogs_serializer(collection_name.find({"_id": ObjectId(id)}))
    return  {"status":"ok","data":blog}


