# Normal way
def Blog_serializer(item) -> dict:
    return {
        "id":str(item["_id"]),
        "title":item["title"],
        "content":item["content"],
        "author":item["author"], 
        "upVoteNumber":item["upVoteNumber"], 
        "downVoteNumber":item["downVoteNumber"], 
    }

def blogs_serializer(entity) -> list:
    return [Blog_serializer(item) for item in entity]
